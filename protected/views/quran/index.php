<?php
/* @var $this QuranController */

$this->breadcrumbs=array(
	'Quran',
);
?>

<?php echo CHtml::beginForm('/mushaf/index.php/quran', 'post', array('class'=>'form-inline')); ?>
	<div class="form-group">
		<?php 
		$surat = array(
			'1' => 'Al-Fatihah',
			'2' => 'Al-Baqarah',
			'3' => 'Ali Imran',
		);
		echo CHtml::dropDownList('surat',1,$surat,array('class'=>'form-control')); ?>
	</div>
	
	<div class="form-group">
		<?php echo CHtml::textField('ayat-first', null, array(
			'class'			=> 'form-control', 
			'placeholder' 	=> 'All ayat',
			'id'			=> 'ayat-first'
		)); ?>
	</div>
	
	<div class="form-group">
		<?php echo CHtml::textField('ayat-last', null, array(
			'class'			=> 'form-control', 
			'placeholder' 	=> 'All ayat',
			'id'			=> 'ayat-last'
		)); ?>
	</div>
	
	<div class="form-group">
		<?php echo CHtml::submitButton('Go', array('class' => 'btn btn-default')); ?>
	</div>
	
<?php echo CHtml::endForm(); ?>

<div class="row">
<?php foreach($quran as $v):?>
	<h3 class="text-right col-md-11">(<?=$v['ayat']?>) <?=$v['arabic_script']?><hr/></h3>
<?php endforeach; ?>
</div>
<script>
	$(document).ready(function(){
	
		$('#ayat-last').prop('disabled', true);
		
		$('#ayat-first').blur(function(){
			if($(this).val().length > 0) {
				$('#ayat-last').prop('disabled', false);
			}else{
				$('#ayat-last').prop('disabled', true);
			}
		});
	});
</script>

<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/bootstrap/dist/css/bootstrap.css" media="screen, projection" />
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.2.js" type="text/javascript"></script>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<div class="container" id="page" class="container">	
		
		<nav class="navbar" role="navigation">
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
						array('label'=>'Home', 'url'=>array('/site/index')),
						array('label'=>'Quran', 'url'=>array('/quran')),
						array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
						array('label'=>'Contact', 'url'=>array('/site/contact')),
						array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
					'htmlOptions' => array(
						'class' => 'nav navbar-nav'
					)
				)); ?>
			</div>
		</nav>
		
		<?php if(isset($this->breadcrumbs)):?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
			)); ?>
		<?php endif?>

		<?php echo $content; ?>

		<div class="row">
			<hr/>
			<p class="text-center">Copyright &copy; <?php echo date('Y'); ?> by <a href="padake.com">padake.com</a></p>
			<p class="text-center">All Rights Reserved.</p>
		</div>

	</div>

</body>
</html>

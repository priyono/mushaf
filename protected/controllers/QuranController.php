<?php

class QuranController extends Controller
{
	public function actionIndex()
	{
		print_r($_POST);
		$first = isset($_POST['ayat-first']) ? $_POST['ayat-first'] : 0;
		$last = isset($_POST['ayat-last']) ? $_POST['ayat-last'] : 0;
		$surat = isset($_POST['surat']) ? $_POST['surat'] : 1;
		$query = 'SELECT * FROM quran_arabic WHERE surat=' . $surat;
		$query .= $first == 0 ? '' : ' LIMIT ' . $first . ', ' . $last;
		echo $query;
		$quran = Yii::app()->db
			->createCommand($query)
			->queryAll();
		$data = new CArrayDataProvider($quran);
		$this->render('index', array(
			'quran' => $data->getData(),
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
